import numpy as np
from utils.constant import KEY_FRECT, KEY_CONFIDENCE, LBL_LYING, LBL_STANDING, LBL_SITTING, LBL_UNKNOWN


def det_classify(img, objects):
    # show_img = img.copy()

    max_w, max_h = 0, 0
    max_score = 0
    rect = None
    if isinstance(objects, list):
        img_h, img_w = img.shape[:2]
        for obj in objects:
            (x, y, x2, y2) = (obj[KEY_FRECT] * np.array([img_w, img_h, img_w, img_h])).astype(np.int)

            # label = obj[KEY_LABEL]
            confidence = float(obj[KEY_CONFIDENCE])

            if (abs(x2 - x) * abs(y2 - y)) > (max_w * max_h):
                max_w = abs(x2 - x)
                max_h = abs(y2 - y)

                max_score = confidence
                rect = obj[KEY_FRECT]
    else:
        max_score = img['score']
        _img = img['image']
        rect = [0, 0, 1., 1.]
        max_h, max_w = _img.shape[:2]

    # classify
    if max_w > 1.5 * max_h:
        return LBL_LYING, max_score, rect
    elif max_h > 1.5 * max_w:
        return LBL_STANDING, max_score, rect
    elif 0.9 * max_w < max_h < 1.1 * max_w:
        return LBL_SITTING, max_score, rect
    else:
        return LBL_UNKNOWN, max_score, rect
