import os
import sys
import cv2


from utils.constant import MODEL_DIR, INF


class OpenPoseUtils:

    def __init__(self, mode="MPI", debug=False):
        self.debug = debug

        pose_model_dir = os.path.join(MODEL_DIR, "openpose/pose")
        if mode == "COCO":
            proto_file = os.path.join(pose_model_dir, "coco", "pose_deploy_linevec.prototxt")
            weights_file = os.path.join(pose_model_dir, "coco", "pose_iter_440000.caffemodel")

            print(os.path.exists(proto_file))
            print(os.path.exists(weights_file))

            self.n_points = 18

            self.pose_pairs = [[1, 0], [1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11],
                               [11, 12], [12, 13], [0, 14], [0, 15], [14, 16], [15, 17]]

        elif mode == "MPI":
            proto_file = os.path.join(pose_model_dir, "mpi", "pose_deploy_linevec_faster_4_stages.prototxt")
            weights_file = os.path.join(pose_model_dir, "mpi", "pose_iter_160000.caffemodel")
            self.n_points = 15
            self.pose_pairs = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 14], [14, 8], [8, 9],
                               [9, 10], [14, 11], [11, 12], [12, 13]]

        else:
            print("not recognize mode{}".format(mode))
            sys.exit(1)

        self.net = cv2.dnn.readNetFromCaffe(proto_file, weights_file)

        self.width = 368
        self.height = 368

        self.threshold = 0.1

    def detect(self, img):
        img_h, img_w = img.shape[:2]

        inp_blob = cv2.dnn.blobFromImage(img, 1.0 / 255, (self.width, self.height), (0, 0, 0), swapRB=False, crop=False)
        self.net.setInput(inp_blob)

        output = self.net.forward()

        layer_h, layer_w = output.shape[2:4]

        points = []

        for i in range(self.n_points):
            # confidence map of corresponding body's part.
            prob_map = output[0, i, :, :]

            # Find global maxima of the probMap.
            min_val, prob, min_loc, point = cv2.minMaxLoc(prob_map)

            # Scale the point to fit on the original image
            x = (img_w * point[0]) / layer_w
            y = (img_h * point[1]) / layer_h

            if prob > self.threshold:
                # Add the point to the list if the probability is greater than the threshold
                points.append((int(x), int(y)))
            else:
                points.append((INF, INF))

        return points

    def draw_skeleton(self, img, points):
        skeleton_img = img.copy()
        for pair in self.pose_pairs:
            part_a = pair[0]
            part_b = pair[1]

            if points[part_a] and points[part_b]:
                if points[part_a][0] == INF or points[part_a][0] == INF or points[part_b][0] == INF or \
                        points[part_b][0] == INF:
                    pass
                else:
                    cv2.line(skeleton_img, points[part_a], points[part_b], (0, 255, 255), 2)
                    cv2.circle(skeleton_img, points[part_a], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
        return skeleton_img

    @staticmethod
    def draw_key_points(img, points):
        keypoint_img = img.copy()
        for i, point in enumerate(points):
            if point is None:
                continue
            x, y = point
            if x == INF or y == INF:
                continue
            cv2.circle(keypoint_img, (int(x), int(y)), 8, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
            cv2.putText(keypoint_img, "{}".format(i), (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                        lineType=cv2.LINE_AA)
        return keypoint_img


if __name__ == '__main__':
    from utils.constant import ROOT_DIR
    data_dir = os.path.join(ROOT_DIR, "data/images")

    _image_path = os.path.join(data_dir, "openpose", "sample.jpg")
    opu = OpenPoseUtils(mode="MPI")
    opu.detect(img=cv2.imread(_image_path))
