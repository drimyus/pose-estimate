import sys
import os


from endpoint import PostureClassifier
from utils.constant import IMG_EXTs


pc = PostureClassifier()


def test_image(image_path):
    pc.do_image(image_path=image_path, b_show=True)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.argv.append('data/images/test/sitting2.png')
        # sys.argv.append('data/videos/sample_video.mp4')

    if len(sys.argv) < 2:
        sys.stderr.write("Invalidate Input\n")
        sys.exit(1)
    else:
        path = sys.argv[1]
        if not os.path.exists(path):
            sys.stderr.write("No exist such file {}\n".format(path))
            sys.exit(1)
        elif os.path.splitext(path)[1].lower() in IMG_EXTs:
            test_image(image_path=path)
        else:
            sys.stderr.write("Unknown file format {}\n".format(path))
            sys.exit(1)
